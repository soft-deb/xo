import java.util.Scanner;

public class XO {
    private char[][] table;
    private char currentPlayer;

    public XO() {
        table = new char[3][3];
        currentPlayer = 'X';
        createTarang();
    }

    private void createTarang() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                table[row][col] = ' ';
            }
        }
    }

    public void printTarang() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            System.out.print(": ");
            for (int col = 0; col < 3; col++) {
                System.out.print(table[row][col] + " : ");
            }
            System.out.println("\n-------------");
        }
    }

    public boolean isTableFull() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkWin(char player) {
        for (int row = 0; row < 3; row++) {
            if (table[row][0] == player && table[row][1] == player && table[row][2] == player) {
                return true;
            }
        }
        for (int col = 0; col < 3; col++) {
            if (table[0][col] == player && table[1][col] == player && table[2][col] == player) {
                return true;
            }
        }
        if (table[0][0] == player && table[1][1] == player && table[2][2] == player) {
            return true;
        } else if (table[0][2] == player && table[1][1] == player && table[2][0] == player) {
            return true;
        }

        return false;

    }

    public void startGame() {
        Scanner kb = new Scanner(System.in);
        boolean playAgain = true;

        while (playAgain) {

            boolean gameOver = false;
            createTarang();
            System.out.println("====================");
            System.out.println("=Welcome to XO Game=");
            System.out.println("====================");
            printTarang();
            while (!gameOver) {
                System.out.println("Player " + currentPlayer + "'s Turn");
                System.out.print("Enter row (1-3): ");
                int row = kb.nextInt() - 1;
                System.out.print("Enter column (1-3): ");
                int col = kb.nextInt() - 1;

                if (row < 0 || row >= 3 || col < 0 || col >= 3 || table[row][col] != ' ') {
                    System.out.println("Invalid move. Try again.");
                    continue;
                }

                table[row][col] = currentPlayer;
                System.out.println("--------------------");
                printTarang();

                if (checkWin(currentPlayer)) {
                    System.out.println("Player " + currentPlayer + " wins!");
                    gameOver = true;
                } else if (isTableFull()) {

                    System.out.println("The game ends in a draw!");
                    gameOver = true;
                }

                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';

            }

            while (true) {
                System.out.print("Continue? (y/n): ");
                String choice = kb.next().toLowerCase();
                if (choice.equals("n")) {
                    playAgain = false;
                    break;
                } else if (choice.equals("y")) {
                    break;
                } else {
                    System.out.println("Please enter y or n");
                }


        }

     }kb.close();
} 

    public static void main(String[] args) {
        XO game = new XO();
        game.createTarang();
        game.printTarang();

    }

}